﻿using System;

using UIKit;

namespace Calculator
{
	public partial class ViewController : UIViewController
	{
		protected ViewController (IntPtr handle) : base (handle)
		{
			// Note: this .ctor should not contain any initialization logic.
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			DisplayLabel.Text = "";
		}

		partial void NumberButtonClick (Foundation.NSObject sender)
		{

		}
	}
}
