﻿using System;
namespace Calculator
{
	public class Calculator
	{
		private CalculateOperation _currentOperation;
		private string _currentValue;
		private string _currentInputValue;
		private bool _operationJustSelected;

		public string GetCurrentValue ()
		{
			return _currentInputValue;
		}

		public void InputNumber (string number)
		{
			if (_operationJustSelected)
			{
				_currentInputValue = number;
			}
			else
			{
				_currentInputValue += number;
			}

			_operationJustSelected = false;
		}

		public void Calculate ()
		{
			if (_currentOperation == CalculateOperation.None)
			{
				return;
			}

			decimal numericCurrentValue = Decimal.Parse (_currentValue);
			decimal numericCurrentInputValue = Decimal.Parse (_currentInputValue);

			switch (_currentOperation)
			{
			case CalculateOperation.Add:
				numericCurrentValue += numericCurrentInputValue;
				break;
			case CalculateOperation.Subtract:
				numericCurrentValue -= numericCurrentInputValue;
				break;
			case CalculateOperation.Multiply:
				numericCurrentValue *= numericCurrentInputValue;
				break;
			case CalculateOperation.Divide:
				numericCurrentValue /= numericCurrentInputValue;
				break;
			}

			_currentValue = numericCurrentValue.ToString ();
			_currentInputValue = numericCurrentInputValue.ToString ();
			_currentInputValue = _currentValue;
		}

		public void InputDecimal ()
		{
			if (_currentInputValue.Contains ("."))
			{
				return;
			}

			_currentInputValue += ".";
			_operationJustSelected = false;
		}

		public void OperationSelected (CalculateOperation operation)
		{
			Calculate ();
			_currentValue = _currentInputValue;
			_currentOperation = operation;
			_operationJustSelected = true;
		}

		public void EqualSelected ()
		{
			Calculate ();
			_currentOperation = CalculateOperation.None;
			_operationJustSelected = true;
		}

		public void DeleteSelected ()
		{
			_currentInputValue = _currentInputValue.Remove (_currentInputValue.Length - 1);
		}

		public void Clear ()
		{
			_currentInputValue = "";
			_operationJustSelected = false;
		}
	}

	public enum CalculateOperation
	{
		None,
		Add,
		Subtract,
		Multiply,
		Divide
	}
}
