// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace iOSCalculator
{
	[Register ("ViewController")]
	partial class ViewController
	{
		[Outlet]
		UIKit.UILabel DisplayLabel { get; set; }

		[Action ("ClearButtonClick:")]
		partial void ClearButtonClick (Foundation.NSObject sender);

		[Action ("DecimalButtonClick:")]
		partial void DecimalButtonClick (Foundation.NSObject sender);

		[Action ("DeleteButtonClick:")]
		partial void DeleteButtonClick (Foundation.NSObject sender);

		[Action ("DivideButtonClick:")]
		partial void DivideButtonClick (Foundation.NSObject sender);

		[Action ("EqualButtonClick:")]
		partial void EqualButtonClick (Foundation.NSObject sender);

		[Action ("MinusButtonClick:")]
		partial void MinusButtonClick (Foundation.NSObject sender);

		[Action ("MultiplyButtonClick:")]
		partial void MultiplyButtonClick (Foundation.NSObject sender);

		[Action ("NumberButtonClick:")]
		partial void NumberButtonClick (Foundation.NSObject sender);

		[Action ("PlusButtonClick:")]
		partial void PlusButtonClick (Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (DisplayLabel != null) {
				DisplayLabel.Dispose ();
				DisplayLabel = null;
			}
		}
	}
}
