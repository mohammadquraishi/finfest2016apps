﻿using System;

using UIKit;

namespace iOSCalculator
{
	public partial class ViewController : UIViewController
	{
		private Calculator _calculator;

		protected ViewController (IntPtr handle) : base (handle)
		{
			_calculator = new Calculator ();
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			DisplayLabel.Text = "";
		}

		partial void NumberButtonClick (Foundation.NSObject sender)
		{
			_calculator.InputNumber (((UIButton)sender).TitleLabel.Text);
			DisplayLabel.Text = _calculator.GetCurrentValue ();
		}
		//HANDLERS-------------------------------------------------
		partial void EqualButtonClick (Foundation.NSObject sender)
		{
			_calculator.EqualSelected ();
			DisplayLabel.Text = _calculator.GetCurrentValue ();
		}

		partial void DecimalButtonClick (Foundation.NSObject sender)
		{
			_calculator.InputDecimal ();
			DisplayLabel.Text = _calculator.GetCurrentValue ();
		}

		partial void DivideButtonClick (Foundation.NSObject sender)
		{
			_calculator.OperationSelected (CalculateOperation.Divide);
			DisplayLabel.Text = _calculator.GetCurrentValue ();
		}

		partial void MinusButtonClick (Foundation.NSObject sender)
		{
			_calculator.OperationSelected (CalculateOperation.Subtract);
			DisplayLabel.Text = _calculator.GetCurrentValue ();
		}

		partial void MultiplyButtonClick (Foundation.NSObject sender)
		{
			_calculator.OperationSelected (CalculateOperation.Multiply);
			DisplayLabel.Text = _calculator.GetCurrentValue ();
		}

		partial void PlusButtonClick (Foundation.NSObject sender)
		{
			_calculator.OperationSelected (CalculateOperation.Add);
			DisplayLabel.Text = _calculator.GetCurrentValue ();
		}

		partial void ClearButtonClick (Foundation.NSObject sender)
		{
			_calculator.Clear ();
			DisplayLabel.Text = _calculator.GetCurrentValue ();
		}

		partial void DeleteButtonClick (Foundation.NSObject sender)
		{
			_calculator.DeleteSelected ();
			DisplayLabel.Text = _calculator.GetCurrentValue ();
		}
		//HANDLERS END_---------------------------------------------
	}
}

