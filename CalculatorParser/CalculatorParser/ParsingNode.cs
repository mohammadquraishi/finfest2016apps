﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculatorParser
{
    abstract class ParsingNode
    {
        public ParsingNode Left { get; set; }
        public ParsingNode Right { get; set; } 
    }
}
