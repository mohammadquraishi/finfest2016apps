﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculatorParser
{
    class OperatorNode : ParsingNode
    {

        public static OperatorNode Get(char operation, ParsingNode root)
        {
            return operation == '!' ? new PostPositionalUnaryOperatorNode(operation) : 
                operation == '-' && (root == null || root is OperatorNode) ? new PrePositionalUnaryOperatorNode(operation) : new OperatorNode(operation);
        }
        public char Operator { get; set; }

        public OperatorNode(char operation)
        {
            Operator = operation;
        }
    }
}
