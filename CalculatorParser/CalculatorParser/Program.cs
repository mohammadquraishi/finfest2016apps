﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculatorParser
{
    class Program
    {
        static void Main(string[] args)
        {
            CalculatorModel c = new CalculatorModel();
            while (true)
            {
                var input = Console.ReadLine();
                if(input.Equals("exit", StringComparison.InvariantCultureIgnoreCase))
                {
                    Environment.Exit(0);
                }
                foreach(char key in input)
                {
                    c.Input(key);
                }
                Console.WriteLine(c.Output);
            }
        }
    }
}
