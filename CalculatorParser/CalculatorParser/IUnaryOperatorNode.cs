﻿namespace CalculatorParser
{
    internal interface IUnaryOperatorNode
    {
        int PerformOperation(int operand);
    }
}