﻿using System;

namespace CalculatorParser
{
    class CalculatorTree
    {
        private const string ERROR = "Syntax!";
        internal ParsingNode Root { get; private set; }

        public void InsertOperator(char operatorChracter)
        {
            var node = OperatorNode.Get(operatorChracter, Root);
            InsertNode(Root, node);
        }

        public void InsertOperand(int operand)
        {
            var node = new OperandNode(operand);
            InsertNode(Root, node);
        }

        void InsertNode(ParsingNode root, ParsingNode node)
        {
            if(root == null)
            {
                if(node is OperatorNode || node is PostPositionalUnaryOperatorNode)
                {
                    throw new ArgumentException(ERROR);
                }
                else
                {
                    Root = node;
                }
            }
            else if(root is OperandNode)
            {
                if (node is PostPositionalUnaryOperatorNode)
                {
                    (root as OperandNode).Operand = (node as PostPositionalUnaryOperatorNode).PerformOperation((root as OperandNode).Operand);
                }
                else if (node is OperatorNode)
                {
                    node.Left = root;
                    Root = node;
                }
                else
                {
                    throw new ArgumentException(ERROR);
                }
            }
            else if(root is OperatorNode)
            {
                if(node is PrePositionalUnaryOperatorNode)
                {
                    if(root.Right == null)
                    {
                        root.Right = node;
                    }
                    else
                    {
                        InsertNode(root.Right, node);
                    }
                }
                else if(node is PostPositionalUnaryOperatorNode)
                {
                    InsertNode(root.Right, node);
                }
                else if(node is OperatorNode)
                {
                    if (!DoesPrecede(root as OperatorNode, node as OperatorNode))
                    {
                        node.Left = root;
                        Root = node;
                    }
                    else
                    {
                        node.Left = root.Right;
                        root.Right = node;
                    }
                }
                else
                {
                    if(root.Right == null)
                    {
                        root.Right = node;
                    }
                    else if(root.Right is PrePositionalUnaryOperatorNode)
                    {
                        (node as OperandNode).Operand = (root.Right as PrePositionalUnaryOperatorNode).PerformOperation((node as OperandNode).Operand);
                        root.Right = node;
                    }
                    else
                    {
                        InsertNode(root.Right, node);
                    }
                }
            }
        }

        public int Calculate()
        {
            var result = Calculate(Root);
            Root = null;
            return result;
        }

        int Calculate(ParsingNode node)
        {
            if(node.Right == null  && node.Left == null)
            {
                return (node as OperandNode).Operand;
            }
            var operation = (node as OperatorNode).Operator;
            switch(operation)
            {
                case '+':
                    return Calculate(node.Left) + Calculate(node.Right);
                case '-':
                    return Calculate(node.Left) - Calculate(node.Right);
                case '*':
                    return Calculate(node.Left) * Calculate(node.Right);
                case '/':
                    return Calculate(node.Left) / Calculate(node.Right);
                case '^':
                    return (int)Math.Pow(Calculate(node.Left), Calculate(node.Right));
            }
            throw new ArgumentException("Syntax!");
        }

        int GetPrecedence(char operation)
        {
            if (operation == '+' || operation == '-')
            {
                return 1;
            }
            if(operation == '*' || operation == '/')
            {
                return 2;
            }
            return 3;
        }

        bool DoesPrecede(OperatorNode first, OperatorNode second)
        {
            return GetPrecedence(second.Operator) > GetPrecedence(first.Operator);
        }
    }
}
