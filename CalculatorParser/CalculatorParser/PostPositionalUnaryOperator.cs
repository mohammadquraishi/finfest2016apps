﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculatorParser
{
    class PostPositionalUnaryOperatorNode : OperatorNode, IUnaryOperatorNode
    {
        public PostPositionalUnaryOperatorNode(char operation)
            : base(operation)
        {

        }

        public int PerformOperation(int operand)
        {
            for(int i = operand - 1; i > 0; i--)
            {
                operand *= i;
            }
            return operand;
        }
    }
}
