﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CalculatorParser
{
    class CalculatorModel
    {
        const string VALID_EXPRESSION_PATTERN = @"(\-?[0-9]*!?[!|\+|\-|\*|\/|^]{1}\-?[0-9]*!?)*";
        const string OPERATOR_PATTERN = @"[|!|\-|\+|*|\/|^]{1}";
        const string OPERAND_PATTERN = @"(\-[0-9]|[0-9])*";

        public string Expression
        {
            get
            {
                return _expressionBuilder.ToString();
            }
        }
        public bool ExpressionIsValid { get; private set; }
        public string Output { get; private set; }
        private StringBuilder _expressionBuilder;
        private Regex _expressionRegex;
        private Regex _operatorRegex;
        private Regex _operandRegex;
        private CalculatorTree _calculator;

        public CalculatorModel()
        {
            _expressionBuilder = new StringBuilder();
            _operandRegex = new Regex(OPERAND_PATTERN);
            _operatorRegex = new Regex(OPERATOR_PATTERN);
            _expressionRegex = new Regex(VALID_EXPRESSION_PATTERN);
            _calculator = new CalculatorTree();
        }
        public void Input(char key)
        {
            var expression = _expressionBuilder.ToString();
            var match = _expressionRegex.Match(expression);
            ExpressionIsValid = match.Success && match.Value.Length == expression.Length;
            if (key == '=' && ExpressionIsValid)
            {
                int i = 0;
                while(expression.Length > 0)
                {
                    var operand = i % 2 == 0;
                    match = operand ? _operandRegex.Match(expression) : _operatorRegex.Match(expression);
                    if(match.Success)
                    {
                        var term = match.Value;
                        if(operand)
                        {
                            _calculator.InsertOperand(Int32.Parse(term));
                        }
                        else
                        {
                            _calculator.InsertOperator(term[0]);
                        }
                    }
                    else
                    {
                        throw new ArgumentException();
                    }
                    expression = expression.Substring(match.Index + match.Length);
                    if (match.Value[0] !='!')
                    {
                        i++;
                    }
                }
                Output = _calculator.Calculate().ToString();
                _expressionBuilder = new StringBuilder();
            }
            else
            { 
                _expressionBuilder.Append(key);
            }
        }
    }
}
