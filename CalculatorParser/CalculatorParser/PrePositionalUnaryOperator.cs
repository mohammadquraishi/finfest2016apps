﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculatorParser
{
    class PrePositionalUnaryOperatorNode : OperatorNode, IUnaryOperatorNode
    {
        public PrePositionalUnaryOperatorNode(char operation)
            :base(operation)
        {

        }

        public int PerformOperation(int operand)
        {
            return operand * -1;
        }
    }
}
