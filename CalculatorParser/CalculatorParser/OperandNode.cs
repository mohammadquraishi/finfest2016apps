﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculatorParser
{
    class OperandNode : ParsingNode
    {
        public OperandNode(int operand)
        {
            Operand = operand;
        }

        public int Operand { get; set; }
    }
}
