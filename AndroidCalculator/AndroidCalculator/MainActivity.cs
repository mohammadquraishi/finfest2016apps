﻿using Android.App;
using Android.Widget;
using Android.OS;

namespace AndroidCalculator
{
	[Activity (Label = "AndroidCalculator", MainLauncher = true, Icon = "@mipmap/icon")]
	public class MainActivity : Activity
	{
		private Calculator _calculator;

		private Button _number0;
		//OTHER BUTTONS ---------------------------------------
		private Button _number1;
		private Button _number2;
		private Button _number3;
		private Button _number4;
		private Button _number5;
		private Button _number6;
		private Button _number7;
		private Button _number8;
		private Button _number9;

		private Button _multiply;
		private Button _divide;
		private Button _add;
		private Button _subtract;

		private Button _decimal;
		private Button _equal;

		private Button _delete;
		private Button _clear;
		//OTHER BUTTONS END -----------------------------------
		private TextView _displayView;

		protected override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.Main);

			// Get our button from the layout resource,
			// and attach an event to it

			_number0 = FindViewById<Button> (Resource.Id.calc_button0);
			_number1 = FindViewById<Button> (Resource.Id.calc_button1);
			_number2 = FindViewById<Button> (Resource.Id.calc_button2);
			_number3 = FindViewById<Button> (Resource.Id.calc_button3);
			_number4 = FindViewById<Button> (Resource.Id.calc_button4);
			_number5 = FindViewById<Button> (Resource.Id.calc_button5);
			_number6 = FindViewById<Button> (Resource.Id.calc_button6);
			_number7 = FindViewById<Button> (Resource.Id.calc_button7);
			_number8 = FindViewById<Button> (Resource.Id.calc_button8);
			_number9 = FindViewById<Button> (Resource.Id.calc_button9);
			_multiply = FindViewById<Button> (Resource.Id.calc_button_multiplication);
			_subtract = FindViewById<Button> (Resource.Id.calc_button_minus);
			_divide = FindViewById<Button> (Resource.Id.calc_button_division);
			_add = FindViewById<Button> (Resource.Id.calc_button_add);
			_decimal = FindViewById<Button> (Resource.Id.calc_button_decimal);
			_equal = FindViewById<Button> (Resource.Id.calc_button_equals);

			_delete = FindViewById<Button> (Resource.Id.delete_button);
			_clear = FindViewById<Button> (Resource.Id.clear_button);
			_displayView = FindViewById<TextView> (Resource.Id.display_label);

			_calculator = new Calculator ();
		}

		protected override void OnResume ()
		{
			base.OnResume ();

			_number0.Click += NumberButtonClicked;
			//OTHER HANDLERS ----------------------------------
			_number1.Click += NumberButtonClicked;
			_number2.Click += NumberButtonClicked;
			_number3.Click += NumberButtonClicked;
			_number4.Click += NumberButtonClicked;
			_number5.Click += NumberButtonClicked;
			_number6.Click += NumberButtonClicked;
			_number7.Click += NumberButtonClicked;
			_number8.Click += NumberButtonClicked;
			_number9.Click += NumberButtonClicked;

			_decimal.Click += DecimalClicked;
			_equal.Click += EqualClicked;
			_add.Click += AddClicked;
			_subtract.Click += SubtractClicked;
			_divide.Click += DivideClicked;
			_multiply.Click += MultiplyClicked;
			_delete.Click += DeleteClicked;
			_clear.Click += ClearClicked;
			//OTHER HANDLERS END -------------------------------

		}

		private void NumberButtonClicked (object sender, System.EventArgs e)
		{
			_calculator.InputNumber (((Button)sender).Text);
			_displayView.Text = _calculator.GetCurrentValue ();
		}

		// OTHER HANDLERS--------------------------------------
		private void DecimalClicked (object sender, System.EventArgs e)
		{
			_calculator.InputDecimal ();
			_displayView.Text = _calculator.GetCurrentValue ();
		}

		private void EqualClicked (object sender, System.EventArgs e)
		{
			_calculator.EqualSelected ();
			_displayView.Text = _calculator.GetCurrentValue ();
		}

		private void AddClicked (object sender, System.EventArgs e)
		{
			_calculator.OperationSelected (CalculateOperation.Add);
			_displayView.Text = _calculator.GetCurrentValue ();
		}

		private void SubtractClicked (object sender, System.EventArgs e)
		{
			_calculator.OperationSelected (CalculateOperation.Subtract);
			_displayView.Text = _calculator.GetCurrentValue ();
		}

		private void MultiplyClicked (object sender, System.EventArgs e)
		{
			_calculator.OperationSelected (CalculateOperation.Multiply);
			_displayView.Text = _calculator.GetCurrentValue ();
		}

		private void DivideClicked (object sender, System.EventArgs e)
		{
			_calculator.OperationSelected (CalculateOperation.Divide);
			_displayView.Text = _calculator.GetCurrentValue ();
		}

		private void DeleteClicked (object sender, System.EventArgs e)
		{
			_calculator.DeleteSelected ();
			_displayView.Text = _calculator.GetCurrentValue ();
		}

		private void ClearClicked (object sender, System.EventArgs e)
		{
			_calculator.Clear ();
			_displayView.Text = _calculator.GetCurrentValue ();
		}
		// OTHER HANDLERS END--------------------------------------

	}
}


