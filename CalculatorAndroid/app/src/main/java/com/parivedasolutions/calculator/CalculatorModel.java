package com.parivedasolutions.calculator;

/**
 * Created by mohammadquraishi on 9/2/16.
 */
public class CalculatorModel {
    private int result = 0;
    private String output = "0";
    public final char PLUS = '+';
    public final char SUBTRACT = '-';
    public final char MULTIPLY = '*';
    public final char DIVIDE = '/';
    public final char EQUAL = '=';
    private char currentOperator = '@';
    private String operand1 = "";
    private String operand2 ="";

    public void delete() {
        if(operand2 != "") {
            operand2 = operand2.substring(0, operand2.length() - 1);
        } else {
            operand1 = operand1.substring(0, operand1.length() - 1);
        }
    }

    public void clear(){
        currentOperator = '@';
        operand2 = "";
        operand1 = "";
        output = "0";
    }

    public String getOutput() {
        output = operand2 == "" ? operand1 : operand2;
        return output;
    }

    public void inputKey(char key){
        boolean isOpertaor = key == PLUS || key == SUBTRACT || key == MULTIPLY || key == DIVIDE || key == EQUAL;
        if(isOpertaor){
            if(operand1 == ""){
                return;
            } else {
                if(operand1 != "" && operand2 != ""){
                    calculate();
                }
                if(key != EQUAL){
                    currentOperator = key;
                } else {
                    currentOperator = '@';
                }
                return;
            }
        } else {
            if(currentOperator == '@'){
                operand1 += key;
                output = operand1;
            } else {
                operand2 += key;
                output = operand2;
            }

        }
    }

    private void calculate(){
        if(currentOperator == '@'){
            return;
        }
        int a = Integer.parseInt(operand1);
        int b = Integer.parseInt(operand2);
        switch (currentOperator) {
            case PLUS:
                output = (a + b) + "";
                break;
            case SUBTRACT:
                output = (a - b) + "";
                break;
            case DIVIDE:
                output = (a / b) + "";
                break;
            case MULTIPLY:
                output = (a * b) + "";
                break;
            default:
                return;
        }
        result = Integer.parseInt(output);
        operand1 = output;
        operand2 = "";
    }
}
