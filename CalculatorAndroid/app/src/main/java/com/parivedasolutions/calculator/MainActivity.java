package com.parivedasolutions.calculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.calc_button1)
    Button oneKey;
    @BindView(R.id.calc_button2)
    Button twoKey;
    @BindView(R.id.calc_button3)
    Button threeKey;
    @BindView(R.id.calc_button4)
    Button fourKey;
    @BindView(R.id.calc_button5)
    Button fiveKey;
    @BindView(R.id.calc_button6)
    Button sixKey;
    @BindView(R.id.calc_button7)
    Button sevenKey;
    @BindView(R.id.calc_button8)
    Button eightKey;
    @BindView(R.id.calc_button9)
    Button nineKey;
    @BindView(R.id.calc_button0)
    Button zeroKey;
    @BindView(R.id.calc_button_add)
    Button addKey;
    @BindView(R.id.calc_button_minus)
    Button minusKey;
    @BindView(R.id.calc_button_multiplication)
    Button multiplyKey;
    @BindView(R.id.calc_button_division)
    Button divideKey;
    @BindView(R.id.calc_button_equals)
    Button equalKey;
    @BindView(R.id.delete_button)
    Button delKey;
    @BindView(R.id.clear_button)
    Button clearKey;
    @BindView(R.id.display_label)
    TextView output;
    CalculatorModel cmodel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cmodel = new CalculatorModel();
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.delete_button)
    void delKeyPressed(){
        cmodel.delete();
        output.setText(cmodel.getOutput());
    }

    @OnClick(R.id.clear_button)
    void clearKeyPressed(){
        cmodel.clear();
        output.setText(cmodel.getOutput());
    }

    @OnClick(R.id.calc_button1)
    void oneKeyPressed() {
        cmodel.inputKey('1');
        output.setText(cmodel.getOutput());
    }

    @OnClick(R.id.calc_button2)
    void twoKeyPressed() {
        cmodel.inputKey('2');
        output.setText(cmodel.getOutput());
    }

    @OnClick(R.id.calc_button3)
    void threeKeyPressed() {
        cmodel.inputKey('3');
        output.setText(cmodel.getOutput());
    }

    @OnClick(R.id.calc_button4)
    void fourKeyPressed() {
        cmodel.inputKey('4');
        output.setText(cmodel.getOutput());
    }

    @OnClick(R.id.calc_button5)
    void fiveKeyPressed() {
        cmodel.inputKey('5');
        output.setText(cmodel.getOutput());
    }

    @OnClick(R.id.calc_button6)
    void sixKeyPressed() {
        cmodel.inputKey('6');
        output.setText(cmodel.getOutput());
    }

    @OnClick(R.id.calc_button7)
    void sevenKeyPressed() {
        cmodel.inputKey('7');
        output.setText(cmodel.getOutput());
    }

    @OnClick(R.id.calc_button8)
    void eightKeyPressed() {
        cmodel.inputKey('8');
        output.setText(cmodel.getOutput());
    }

    @OnClick(R.id.calc_button9)
    void nineKeyPressed() {
        cmodel.inputKey('9');
        output.setText(cmodel.getOutput());
    }

    @OnClick(R.id.calc_button0)
    void zeroKeyPressed() {
        cmodel.inputKey('0');
        output.setText(cmodel.getOutput());
    }

    @OnClick(R.id.calc_button_add)
    void addKeyPressed() {
        cmodel.inputKey(cmodel.PLUS);
        output.setText(cmodel.getOutput());
    }

    @OnClick(R.id.calc_button_minus)
    void minusKeyPressed() {
        cmodel.inputKey(cmodel.SUBTRACT);
        output.setText(cmodel.getOutput());
    }

    @OnClick(R.id.calc_button_multiplication)
    void multiplyKeyPressed() {
        cmodel.inputKey(cmodel.MULTIPLY);
        output.setText(cmodel.getOutput());
    }

    @OnClick(R.id.calc_button_division)
    void divideKeyPressed(){
        cmodel.inputKey(cmodel.DIVIDE);
        output.setText(cmodel.getOutput());
    }

    @OnClick(R.id.calc_button_equals)
    void equalKeyPressed(){
        cmodel.inputKey(cmodel.EQUAL);
        output.setText(cmodel.getOutput());
    }
}
