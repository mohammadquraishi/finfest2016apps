//
//  ViewController.swift
//  Calculator
//
//  Created by Mohammad Quraishi on 8/7/16.
//  Copyright © 2016 Pariveds Solutions. All rights reserved.
//

import UIKit

class ViewController: UIViewController {


    @IBOutlet weak var PlusKey: UIButton!
    @IBOutlet weak var MultiplyKey: UIButton!

    @IBOutlet weak var DelButton: UIButton!
    @IBOutlet weak var EqualKey: UIButton!
    @IBOutlet weak var DivideKey: UIButton!

    @IBOutlet weak var ClearButton: UIButton!
    @IBOutlet weak var MinusKey: UIButton!
    
    @IBOutlet weak var TwoKey: UIButton!
    @IBOutlet weak var ResultLabel: UILabel!
    @IBOutlet weak var Display: UILabel!
    @IBOutlet weak var SevenKey: UIButton!
    @IBOutlet weak var ThreeKey: UIButton!
    @IBOutlet weak var SixKey: UIButton!
    @IBOutlet weak var ZeroKey: UIButton!
    @IBOutlet weak var FiveKey: UIButton!
    @IBOutlet weak var FourKey: UIButton!
    @IBOutlet weak var DecimalKey: UIButton!
    @IBOutlet weak var NineKey: UIButton!
    @IBOutlet weak var OneKey: UIButton!
    @IBOutlet weak var EightKey: UIButton!
    fileprivate var cmodel = CalculatorModel()
    
    override init(nibName nibNameOrNil: String?,
                          bundle nibBundleOrNil: Bundle?)
    {
        cmodel = CalculatorModel()
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //keys
        OneKey.addTarget(self, action: #selector(ViewController.oneKeyPressed), for: UIControlEvents.touchUpInside);
        TwoKey.addTarget(self, action: #selector(ViewController.twoKeyPressed), for: UIControlEvents.touchUpInside);
        ThreeKey.addTarget(self, action: #selector(ViewController.threeKeyPressed), for: UIControlEvents.touchUpInside);
        FourKey.addTarget(self, action: #selector(ViewController.fourKeyPressed), for: UIControlEvents.touchUpInside);
        FiveKey.addTarget(self, action: #selector(ViewController.fiveKeyPressed), for: UIControlEvents.touchUpInside);
        SixKey.addTarget(self, action: #selector(ViewController.sixKeyPressed), for: UIControlEvents.touchUpInside);
        SevenKey.addTarget(self, action: #selector(ViewController.sevenKeyPressed), for: UIControlEvents.touchUpInside);
        EightKey.addTarget(self, action: #selector(ViewController.eightKeyPressed), for: UIControlEvents.touchUpInside);
        NineKey.addTarget(self, action: #selector(ViewController.nineKeyPressed), for: UIControlEvents.touchUpInside);
        ZeroKey.addTarget(self, action: #selector(ViewController.zeroKeyPressed), for: UIControlEvents.touchUpInside);
        //operators
        PlusKey.addTarget(self, action: #selector(ViewController.addKeyPressed), for: UIControlEvents.touchUpInside)
        MinusKey.addTarget(self, action: #selector(ViewController.subtractKeyPressed), for: UIControlEvents.touchUpInside)
        MultiplyKey.addTarget(self, action: #selector(ViewController.multiplyKeyPressed), for: UIControlEvents.touchUpInside)
        DivideKey.addTarget(self, action: #selector(ViewController.divideKeyPressed), for: UIControlEvents.touchUpInside)
        EqualKey.addTarget(self, action: #selector(ViewController.equalKeyPressed), for: UIControlEvents.touchUpInside)
        //operations
        DelButton.addTarget(self, action: #selector(ViewController.delKeyPressed), for: UIControlEvents.touchUpInside)
        ClearButton.addTarget(self, action: #selector(ViewController.clearKeyPressed), for: UIControlEvents.touchUpInside)
        Display.text = cmodel.getOutput()
        //operators
    }
    
    func delKeyPressed() {
        cmodel.delete()
        Display.text = cmodel.getOutput()
    }
    
    func clearKeyPressed(){
        cmodel.clear()
        Display.text = cmodel.getOutput()
    }
    
    func oneKeyPressed(){
        cmodel.inputKey("1")
        Display.text = cmodel.getOutput()
    }
    
    func twoKeyPressed(){
        cmodel.inputKey("2")
        Display.text = cmodel.getOutput()
    }
    
    func threeKeyPressed(){
        cmodel.inputKey("3")
        Display.text = cmodel.getOutput()
    }
    
    func fourKeyPressed(){
        cmodel.inputKey("4")
        Display.text = cmodel.getOutput()
    }
    
    func fiveKeyPressed(){
        cmodel.inputKey("5")
        Display.text = cmodel.getOutput()
    }
    
    func sixKeyPressed(){
        cmodel.inputKey("6")
        Display.text = cmodel.getOutput()
    }
    
    func sevenKeyPressed(){
        cmodel.inputKey("7")
        Display.text = cmodel.getOutput()
    }
    
    func eightKeyPressed(){
        cmodel.inputKey("8")
        Display.text = cmodel.getOutput()
    }
    
    func nineKeyPressed(){
        cmodel.inputKey("9")
        Display.text = cmodel.getOutput()
    }
    
    func zeroKeyPressed(){
        cmodel.inputKey("0")
        Display.text = cmodel.getOutput()
    }
    
    func addKeyPressed(){
        cmodel.inputKey(CalculatorModel.plus)
        Display.text = cmodel.getOutput()
    }
    
    func subtractKeyPressed(){
        cmodel.inputKey(CalculatorModel.subtract)
        Display.text = cmodel.getOutput()
    }
    
    func multiplyKeyPressed(){
        cmodel.inputKey(CalculatorModel.multiply)
        Display.text = cmodel.getOutput()
    }
    
    func divideKeyPressed(){
        cmodel.inputKey(CalculatorModel.divide)
        Display.text = cmodel.getOutput()
    }
    
    func equalKeyPressed(){
        cmodel.inputKey(CalculatorModel.equal)
        Display.text = cmodel.getOutput()
    }
}

