//
//  CalculatorModel.swift
//  Calculator
//
//  Created by Mohammad Quraishi on 8/7/16.
//  Copyright © 2016 Pariveds Solutions. All rights reserved.
//

import Foundation

open class CalculatorModel{
    fileprivate var result = 0
    fileprivate var output = "0"
    static let plus : Character = "+"
    static let subtract : Character = "-"
    static let multiply : Character = "*"
    static let divide : Character = "/"
    static let equal : Character = "="
    fileprivate var currentOpertator : Character = "@";
    fileprivate var operand1 : String = ""
    fileprivate var operand2 : String = ""
    
    open func delete(){
        if(operand2 != ""){
            operand2 = operand2.substring(with: (operand2.startIndex ..< operand2.characters.index(before: operand2.endIndex)))
        } else {
            operand1 = operand1.substring(with: (operand1.startIndex ..< operand1.characters.index(before: operand1.endIndex)))
        }
    }
    
    
    open func clear(){
        currentOpertator = "@";
        operand1 = ""
        operand2 = ""
        output = "0"
    }
    
    open func getOutput() -> String{
        output = operand2 == "" ? operand1 : operand2
        return output
    }
    
    
    open func inputKey(_ key : Character){
        let isOperator = key == CalculatorModel.plus || key == CalculatorModel.subtract || key == CalculatorModel.multiply || key == CalculatorModel.divide || key == CalculatorModel.equal
        if(isOperator){
            if(operand1 == ""){
                return
            }
            else{
                if(operand1 != "" && operand2 != ""){
                    calculate()
                    
                }
                if(key != CalculatorModel.equal){
                    currentOpertator = key;
                }
                else{
                    currentOpertator = "@"
                }
                return
            }
        }
        else{
            if(currentOpertator == "@"){
                operand1.append(key)
                output = operand1
            }
            else{
                operand2.append(key)
                output = operand2
            }
        }
    }
    
    fileprivate func calculate(){
        if(currentOpertator == "@"){
            return;
        }
        let a:Int? = Int(operand1)
        let b:Int? = Int(operand2)
        switch currentOpertator{
            case CalculatorModel.plus:
                output = String(a! + b!)
            case CalculatorModel.subtract:
                output = String(a! - b!)
            case CalculatorModel.multiply:
                output = String(a! * b!)
            case CalculatorModel.divide:
                output = String(a! / b!)
            default:
                return
        }
        result = Int(output)!;
        operand1 = String(output)
        operand2 = ""
    }
}
