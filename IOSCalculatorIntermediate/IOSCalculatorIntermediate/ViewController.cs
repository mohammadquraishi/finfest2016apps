﻿using System;

using UIKit;

namespace IOSCalculatorIntermediate
{
	public partial class ViewController : UIViewController
	{
		protected ViewController (IntPtr handle) : base (handle)
		{
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			DisplayLabel.Text = "";
		}

		partial void NumberButtonClick (Foundation.NSObject sender)
		{
			DisplayLabel.Text = ((UIButton)sender).TitleLabel.Text;
		}

		partial void EqualButtonClick (Foundation.NSObject sender)
		{
			DisplayLabel.Text = ((UIButton)sender).TitleLabel.Text;
		}

		partial void DecimalButtonClick (Foundation.NSObject sender)
		{
			DisplayLabel.Text = ((UIButton)sender).TitleLabel.Text;
		}

		partial void DivideButtonClick (Foundation.NSObject sender)
		{
			DisplayLabel.Text = ((UIButton)sender).TitleLabel.Text;
		}

		partial void MinusButtonClick (Foundation.NSObject sender)
		{
			DisplayLabel.Text = ((UIButton)sender).TitleLabel.Text;
		}

		partial void MultiplyButtonClick (Foundation.NSObject sender)
		{
			DisplayLabel.Text = ((UIButton)sender).TitleLabel.Text;
		}

		partial void PlusButtonClick (Foundation.NSObject sender)
		{
			DisplayLabel.Text = ((UIButton)sender).TitleLabel.Text;
		}

		partial void ClearButtonClick (Foundation.NSObject sender)
		{
			DisplayLabel.Text = ((UIButton)sender).TitleLabel.Text;
		}

		partial void DeleteButtonClick (Foundation.NSObject sender)
		{
			DisplayLabel.Text = ((UIButton)sender).TitleLabel.Text;
		}
	}
}
